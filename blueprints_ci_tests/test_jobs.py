import os
import subprocess
import yaml

import logging
from unittest import TestCase
from unittest.mock import Mock
from blueprints_ci.test import write_testenv
from blueprints_ci import (
    generate_lava_job_definition,
    send_testjob_request_to_squad,
    resolve_os_or_firmware,
    register_callback_in_squad,
    resolve_test_plan,
    Settings,
)


logger = logging.getLogger()


def get_git_rev_short_hash():
    return subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD']).decode('ascii').strip()


def sample_settings():
    """
    There is no optimal way to detect if tests are running on CI or
    locally, so we'll check if CI set any variable and be done with that.
    """

    running_on_ci = os.getenv("CI_API_V4_URL") is not None

    settings = None
    git_hash = "git-hash"
    if running_on_ci:
        settings = Settings(extra=[
            "SQUAD_HOST",
            "SQUAD_TOKEN",
            "SQUAD_GROUP",
            "SQUAD_PROJECT",
        ])
        if settings.missing:
            return None
    else:
        user = os.getenv("USER", "blueprints-ci-dev")
        git_hash = get_git_rev_short_hash()
        settings = Mock(
            SQUAD_GROUP="blueprints",
            SQUAD_PROJECT="ci",
            SQUAD_HOST=os.getenv("SQUAD_HOST"),
            NIGHTLYBUILDS_URL="https://gitlab.com/Linaro/blueprints/nightly-builds",
            CI_COMMIT_SHORT_SHA=git_hash,
            CI_PROJECT_DIR=".",
            CI_JOB_NAME=f"{user}-local-test",
            IMAGES_DIR="images",
            SUBMIT_JOBS=(os.getenv("SUBMIT_JOBS") == "1"),
            SKIP=None,
            TESTS=None,
        )

    settings.RUNNING_ON_CI = running_on_ci
    settings.SQUAD_BUILD = os.getenv("CI_COMMIT_SHORT_SHA") or git_hash
    return settings


class TestJobs(TestCase):
    job_ids = []
    settings = None

    def do_test(self, settings, context, os=True, firmware=True):
        if os:
            self.assertIsNotNone(context["os_url"])
        if firmware:
            self.assertIsNotNone(context["firmware_url"])

        definition = generate_lava_job_definition(settings, context=context)
        yaml.safe_load(definition)
        logger.info("Job definition:" + definition)
        self.assertIsNotNone(definition)

        if settings.SUBMIT_JOBS:
            sent, job_id = send_testjob_request_to_squad(settings, definition)
            self.assertTrue(sent)
            self.assertTrue(int(job_id) > 0)
            TestJobs.job_ids.append(job_id)

    def test_synquacer_job(self):
        settings = TestJobs.settings
        settings.LAVA_DEVICE = "synquacer"
        context = {
            "os_url": resolve_os_or_firmware(settings, "build-meta-trs:trs-image-trs-qemuarm64.rootfs.wic.gz"),
            "firmware_url": resolve_os_or_firmware(settings, "build-meta-ts-synquacer:ts-firmware-synquacer.rootfs.tar.gz"),
            "test_plan": resolve_test_plan(settings),
        }

        self.do_test(settings, context)

    def test_qemu_job(self):
        settings = TestJobs.settings
        settings.LAVA_DEVICE = "qemu"
        context = {
            "os_url": resolve_os_or_firmware(settings, "build-meta-trs:trs-image-trs-qemuarm64.rootfs.wic.gz"),
            "firmware_url": resolve_os_or_firmware(settings, "build-meta-ts-qemuarm64-secureboot:flash.bin-qemu.gz"),
            "test_plan": resolve_test_plan(settings),
        }

        self.do_test(settings, context)

    def test_rockpi_job(self):
        settings = TestJobs.settings
        settings.LAVA_DEVICE = "rk3399-rock-pi-4b"
        context = {
            "os_url": resolve_os_or_firmware(settings, "build-meta-trs:trs-image-trs-qemuarm64.rootfs.wic.gz"),
            "firmware_url": resolve_os_or_firmware(settings, "build-meta-ts-rockpi4b:ts-firmware-rockpi4b.rootfs.wic.gz"),
            "test_plan": resolve_test_plan(settings),
        }

        self.do_test(settings, context)

    def test_ava_job(self):
        settings = TestJobs.settings
        settings.LAVA_DEVICE = "ava"
        context = {
            "os_url": resolve_os_or_firmware(
                settings,
                "build-meta-ewaol-machine-avadp:ewaol-baremetal-image-ava.rootfs.wic.bz2"
            ),
            "test_plan": resolve_test_plan(settings),
        }
        self.do_test(settings, context, firmware=False)

    def test_stm32mp157c_dk2_job(self):
        settings = TestJobs.settings
        settings.LAVA_DEVICE = "stm32mp157c-dk2"
        context = {
            "os_url": resolve_os_or_firmware(settings, "http://releases.linaro.org/components/ledge/rp-0.3/ledge-multi-armv7/ledge-qemuarm/ledge-iot-ledge-qemuarm-20211202160629.rootfs.wic.gz"),
            "firmware_url": resolve_os_or_firmware(settings, "build-meta-ts-stm32mp157c-dk2:ts-firmware-stm32mp157c-dk2.rootfs.wic.gz"),
            "test_plan": resolve_test_plan(settings),
        }

        self.do_test(settings, context)

    def test_zynqmp_starter_job(self):
        settings = TestJobs.settings
        settings.LAVA_DEVICE = "kv260"
        context = {
            "os_url": resolve_os_or_firmware(settings, "build-meta-trs:trs-image-trs-qemuarm64.rootfs.wic.gz"),
            "firmware_url": resolve_os_or_firmware(settings, "build-meta-ts-zynqmp-starter:ImageA.bin.gz,ImageB.bin.gz"),
            "test_plan": resolve_test_plan(settings),
        }

        self.do_test(settings, context)

    @classmethod
    def setUpClass(cls):
        cls.settings = sample_settings()

    @classmethod
    def tearDownClass(cls):
        if cls.settings.SUBMIT_JOBS and cls.settings.RUNNING_ON_CI:
            testenv_out = {
                "SQUAD_JOB_ID": ','.join(cls.job_ids),
                "SQUAD_GROUP": cls.settings.SQUAD_GROUP,
                "SQUAD_PROJECT": cls.settings.SQUAD_PROJECT,
            }
            write_testenv(cls.settings, testenv_out)

            register_callback_in_squad(cls.settings)
