import os

from unittest import TestCase
from unittest.mock import Mock, patch
from blueprints_ci.manifest import manifest


class TestManifest(TestCase):

    def sample_settings(self):
        return Mock(
            GIT_URL="http://some.git/url",
            GIT_BRANCH="some-branch",
            CI_COMMIT_TAG=None,
            CI_PROJECT_DIR=".",
            IS_TRS_MANIFEST_REPO=None,
        )

    @patch("blueprints_ci.manifest.get_local_manifest_xml_from_trs_manifest")
    def test_basics(self, mock_get_local_manifest_xml_from_trs_manifest):
        local_manifest_xml_filename = "local-manifest.xml"
        with open(local_manifest_xml_filename, "w") as fp:
            fp.write("local_manifest manifest contents")

        mock_get_local_manifest_xml_from_trs_manifest.return_value = local_manifest_xml_filename
        settings = self.sample_settings()
        self.assertTrue(manifest(settings))
        mock_get_local_manifest_xml_from_trs_manifest.assert_called()
        os.unlink(local_manifest_xml_filename)

    @patch("blueprints_ci.manifest.get_local_manifest_xml_from_trs_manifest")
    def test_overwrite_trs_manifest_url(self, mock_get_local_manifest_xml_from_trs_manifest):
        settings = self.sample_settings()
        settings.IS_TRS_MANIFEST_REPO = 1

        local_manifest_xml_filename = "local-manifest.xml"
        mock_get_local_manifest_xml_from_trs_manifest.return_value = local_manifest_xml_filename
        with open(local_manifest_xml_filename, "w") as fp:
            fp.write("local_manifest manifest contents")

        # Test with MR
        settings.IS_MERGE_REQUEST = True
        settings.CI_MERGE_REQUEST_SOURCE_BRANCH_NAME = "mr-branch-name"
        settings.CI_MERGE_REQUEST_SOURCE_PROJECT_URL = "https://gitlab.com/my-fork-url/trs-manifest"
        self.assertTrue(manifest(settings))
        self.assertTrue(os.path.exists("local-manifest.xml"))
        overwrite_url = f"{settings.CI_MERGE_REQUEST_SOURCE_PROJECT_URL}/-/raw/{settings.CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}/default.xml"
        mock_get_local_manifest_xml_from_trs_manifest.assert_called_with(settings, overwrite={}, overwrite_url=overwrite_url)

        # Test with tag
        settings.IS_MERGE_REQUEST = False
        settings.CI_COMMIT_TAG = "v1.0"
        settings.CI_MERGE_REQUEST_SOURCE_PROJECT_URL = "https://gitlab.com/Linaro/trusted-reference-stack/trs-manifest"
        self.assertTrue(manifest(settings))
        self.assertTrue(os.path.exists("local-manifest.xml"))
        overwrite_url = f"{settings.CI_MERGE_REQUEST_SOURCE_PROJECT_URL}/-/raw/{settings.CI_COMMIT_TAG}/default.xml"
        mock_get_local_manifest_xml_from_trs_manifest.assert_called_with(settings, overwrite={}, overwrite_url=overwrite_url)
